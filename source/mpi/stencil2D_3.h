#ifndef EXERCISE_07_STENCIL3D_H
#define EXERCISE_07_STENCIL3D_H

#include <iostream>
#include <vector>
#include <array>
#include <cmath>
#include <mpi.h>


struct Grid : std::vector<double> {
    int size;

    Grid() : size(0) {
        resize(0);
    }

    explicit Grid(int n) : size(n) {
        resize(n * n);
    }

    double &operator()(int i, int j) {
        return (*this)[i * size + j];
    }

    const double &operator()(int i, int j) const {
        return (*this)[i * size + j];
    }
};


class Stencil2D {
    int pid;
    int pid_master;
    int num_processes;

    int grid_size;

    double north_boundary;
    double east_boundary;
    double south_boundary;
    double west_boundary;

    Grid cells;

    int start_pos_row;
    int start_pos_column;
    int end_pos_row;
    int end_pos_column;

    int num_horizontal;
    int horizontal_size;
    int num_vertical;
    int vertical_size;


public:
    Stencil2D(int pid, int pid_master, int num_processes, int size, int num_horizontal, int num_vertical,
              double north, double east, double south, double west) :
            pid(pid), pid_master(pid_master), num_processes(num_processes),
            grid_size(size + 2), num_horizontal(num_horizontal), num_vertical(num_vertical), north_boundary(north),
            east_boundary(east), south_boundary(south),
            west_boundary(west) {    // +2 for boundaries; operating area is then from 1 to N in both dimensions

        horizontal_size = size / num_horizontal;
        vertical_size = size / num_vertical;

        cells = Grid(grid_size);

        // setup boundaries (without corners, because not needed in 5 point mpi)
        set_boundaries();

        start_pos_row = vertical_size * (int) floor(pid / (double) num_horizontal);
        start_pos_column = horizontal_size * (pid % num_horizontal);

        end_pos_row = start_pos_row + vertical_size;
        end_pos_column = start_pos_column + horizontal_size;
    }

    ~Stencil2D() = default;

    void set_boundaries() {
        for (int i = 1; i < grid_size - 1; i++) {
            cells(0, i) = north_boundary;
            cells(i, grid_size - 1) = east_boundary;
            cells(grid_size - 1, i) = south_boundary;
            cells(i, 0) = west_boundary;
        }
    }

    double jacobi_iteration_2d(const Grid &in, Grid &out, int size) {
        double sum_changes = 0;

        // compute boundary
        sum_changes += jacobi_iteration_border(in, out, size);

        // send boundary
        synchronization_vertical_send(out);
        synchronization_horizontal_send(out);

        // compute inner part boundary
        sum_changes += jacobi_iteration_inner(in, out, size);

        // receive boundary
        synchronization_vertical_recv(out);
        synchronization_horizontal_recv(out);

        return sum_changes;
    }

    void setVal(const Grid &in, Grid &out, int i, int j, double &sum_changes) {
        out(i, j) = (in(i, j) +         // current position
                     in(i - 1, j) +         // position northern to current position
                     in(i + 1, j) +         // position southern to current position
                     in(i, j - 1) +         // position western to current position
                     in(i, j + 1)) / 5;     // position eastern to current position

        sum_changes += std::abs(out(i, j) - in(i, j));
    }

    double jacobi_iteration_inner(const Grid &in, Grid &out, int size) {
        double sum_changes = 0;

        // leave out boundary and ghost cell => +1 (boundary), +2 (ghost cell)
        for (int i = start_pos_row + 2; i < end_pos_row; i++) {
            for (int j = start_pos_column + 2; j < end_pos_column; j++) {
                setVal(in, out, i, j, sum_changes);
            }
        }

        return sum_changes;
    }

    double jacobi_iteration_border(const Grid &in, Grid &out, int size) {
        int i_begin = start_pos_row + 1;
        int j_begin = start_pos_column + 1;
        int i_end = end_pos_row;
        int j_end = end_pos_column;

        double sum_changes = 0;

        // Calculate north and south boundary

        for (int j = start_pos_column + 1; j < end_pos_column + 1; j++) {
            // north_boundary
            setVal(in, out, i_begin, j, sum_changes);

            // south_boundary
            setVal(in, out, i_end, j, sum_changes);
        }

        // Calculate west and east boundary

        for (int i = start_pos_row + 1; i < end_pos_row + 1; i++) {
            // west_boundary
            setVal(in, out, i, j_begin, sum_changes);

            // east_boundary
            setVal(in, out, i, j_end, sum_changes);
        }

        return sum_changes;
    }


    void synchronization_vertical_send(Grid &vect) {
        int tag = 0;

        bool is_up = pid < num_horizontal;
        bool is_down = pid >= num_horizontal * (num_vertical - 1);

        if (num_vertical > 1) {
            // send down - recv up
            if (!is_down) {
                MPI::COMM_WORLD.Isend(&vect(end_pos_row - 1 + 1, start_pos_column + 1), horizontal_size, MPI::DOUBLE,
                                                 pid + num_horizontal, tag);
            }

            if (!is_up) {
                MPI::COMM_WORLD.Isend(&vect(start_pos_row + 1, start_pos_column + 1), horizontal_size, MPI::DOUBLE,
                                                 pid - num_horizontal, tag);
            }
        }
    }

    void synchronization_horizontal_send(Grid &vect) {
        int tag = 0;

        bool is_left = pid % num_horizontal == 0;
        bool is_right = pid % num_horizontal == num_horizontal - 1;

        if (num_horizontal > 1) {
            std::vector<double> my_left(vertical_size);
            std::vector<double> my_right(vertical_size);

            int c = 0;
            for (int i = start_pos_row; i < end_pos_row; i++) {
                my_left[c] = vect(i + 1, start_pos_column + 1);
                my_right[c] = vect(i + 1, end_pos_column - 1 + 1);
                c++;
            }

            std::vector<double> my_left_neigh(vertical_size);
            std::vector<double> my_right_neigh(vertical_size);

            // send right
            if (!is_right) {
                MPI::COMM_WORLD.Isend(&my_right[0], vertical_size, MPI::DOUBLE, pid + 1, tag);
            }

            // send left
            if (!is_left) {
                MPI::COMM_WORLD.Isend(&my_left[0], vertical_size, MPI::DOUBLE, pid - 1, tag);
            }
        }
    }

    void synchronization_vertical_recv(Grid &vect) {
        int tag = 0;

        bool is_up = pid < num_horizontal;
        bool is_down = pid >= num_horizontal * (num_vertical - 1);

        MPI::Request req_down, req_up;

        if (num_vertical > 1) {
            // recv up
            if (!is_up) {
                req_down = MPI::COMM_WORLD.Irecv(&vect(start_pos_row - 1 + 1, start_pos_column + 1), horizontal_size, MPI::DOUBLE,
                                                   pid - num_horizontal, tag);
            }

            //recv down
            if (!is_down) {
                req_up = MPI::COMM_WORLD.Irecv(&vect(end_pos_row + 1, start_pos_column + 1), horizontal_size, MPI::DOUBLE,
                                               pid + num_horizontal, tag);
            }
        }

        //recv
        req_up.Wait();
        req_down.Wait();
    }

    void synchronization_horizontal_recv(Grid &vect) {
        int tag = 0;

        bool is_left = pid % num_horizontal == 0;
        bool is_right = pid % num_horizontal == num_horizontal - 1;

        MPI::Request req_left, req_right;

        if (num_horizontal > 1) {
            std::vector<double> my_left(vertical_size);
            std::vector<double> my_right(vertical_size);

            int c = 0;
            for (int i = start_pos_row; i < end_pos_row; i++) {
                my_left[c] = vect(i + 1, start_pos_column + 1);
                my_right[c] = vect(i + 1, end_pos_column - 1 + 1);
                c++;
            }

            std::vector<double> my_left_neigh(vertical_size);
            std::vector<double> my_right_neigh(vertical_size);

            // recv left
            if (!is_left) {
                req_right = MPI::COMM_WORLD.Irecv(&my_left_neigh[0], vertical_size, MPI::DOUBLE, pid - 1, tag);
            }

            // recv right
            if (!is_right) {
                req_left = MPI::COMM_WORLD.Irecv(&my_right_neigh[0], vertical_size, MPI::DOUBLE, pid + 1, tag);
            }

            // recv
            req_left.Wait();
            req_right.Wait();

            // copy into vertical vectors
            int c1 = 0;
            for (int i = start_pos_row; i < end_pos_row; i++) {
                if (!is_left) {
                    vect(i + 1, start_pos_column) = my_left_neigh[c1];
                }
                if (!is_right) {
                    vect(i + 1, end_pos_column + 1) = my_right_neigh[c1];
                }
                c1++;
            }
        }
    }

    bool iteration(double epsilon_value, const Grid &in, Grid &out) {
        bool break_out = false;

        double sum_changes = jacobi_iteration_2d(in, out, grid_size);


        double total_changes = 0;

        MPI::COMM_WORLD.Reduce(&sum_changes, &total_changes, 1, MPI::DOUBLE, MPI::SUM, pid_master);

        if (pid == pid_master) {
            // std::cout << "Changes: " << total_changes << std::endl;
            if (total_changes < epsilon_value) {
                break_out = true;
            }
        }

        // if master isn't the only participator in the computation, communicate with the others (slaves)
        if (num_processes > 1) {
            MPI::COMM_WORLD.Bcast(&break_out, 1, MPI::BOOL, pid_master);
        }

        return break_out;
    }

    void calculate_stencil(double epsilon_value) {
        double sum_changes = epsilon_value;

        int iterations = 0;
        Grid temp = cells;

        bool break_out;
        while (true) {
            break_out = iteration(epsilon_value, cells, temp);
            iterations++;

            if (break_out) {    // spares unnecessary additional iteration
                cells = temp;
                break;
            }

            break_out = iteration(epsilon_value, temp, cells);
            iterations++;

            if (break_out) {
                break;
            }
        }

        /*if (pid == pid_master) {
            std::cout << "Simulation took " << iterations << " iteration(s)." << std::endl;
        }*/
    }

    // maximum of the absolute values e.g. (-5, 3) -> -5
    static void max_abs(double *in, double *inout, const int *len, MPI::Datatype *dprt) {
        for (int i = 0; i < *len; i++) {
            inout[i] = (std::max(std::abs(in[i]), std::abs(inout[i])) == std::abs(in[i])) ? in[i] : inout[i];
        }
    }

    void synchronize_full() {
        MPI_Op max_abs_op;
        MPI_Op_create((MPI_User_function *) max_abs, 1, &max_abs_op);

        MPI::COMM_WORLD.Allreduce(MPI::IN_PLACE, &cells(0, 0), grid_size * grid_size, MPI::DOUBLE, max_abs_op);

        MPI_Op_free(&max_abs_op);
    }

    const Grid &getCells() const {
        return cells;
    }

    void print_grid() {
        for (int i = 0; i < grid_size; i++) {
            for (int j = 0; j < grid_size; j++) {
                std::cout << cells(i, j) << "\t\t";
            }

            std::cout << "\n";
        }

        std::cout << std::endl;
    }
};

#endif //EXERCISE_07_STENCIL3D_H
