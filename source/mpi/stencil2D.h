#ifndef EXERCISE_07_STENCIL2D_H
#define EXERCISE_07_STENCIL2D_H

#include <iostream>
#include <vector>
#include <array>
#include <cmath>
#include <mpi.h>


struct Grid : std::vector<double> {
    unsigned size;

    Grid() : size(0) {
        resize(0);
    }

    explicit Grid(unsigned n) : size(n) {
        resize(n * n);
    }

    double &operator()(int i, int j) {
        return (*this)[i * size + j];
    }

    const double &operator()(int i, int j) const {
        return (*this)[i * size + j];
    }
};


class Stencil2D {
    int pid;
    int pid_master;
    int num_processes;

    unsigned int grid_size;

    double north_boundary;
    double east_boundary;
    double south_boundary;
    double west_boundary;

    Grid cells;

    double step_size;
    unsigned start_pos;
    unsigned end_pos;

public:
    Stencil2D(int pid, int pid_master, int num_processes, unsigned int size, double north, double east, double south, double west) :
            pid(pid), pid_master(pid_master), num_processes(num_processes),
            grid_size(size + 2), north_boundary(north), east_boundary(east), south_boundary(south), west_boundary(west) {    // +2 for boundaries; operating area is then from 1 to N in both dimensions

        cells = Grid(grid_size);

        // setup boundaries (without corners, because not needed in 5 point mpi)
        set_boundaries();

        step_size = (double) (grid_size - 2) / (double) num_processes;
        start_pos = (unsigned) round(step_size * pid) + 1;
        end_pos = (unsigned) round(step_size * pid + step_size) + 1;
    }

    ~Stencil2D() = default;

    void set_boundaries() {
        for(unsigned i = 1; i < grid_size-1; i++) {
            cells(0, i) = north_boundary;
            cells(i, grid_size-1) = east_boundary;
            cells(grid_size-1, i) = south_boundary;
            cells(i, 0) = west_boundary;
        }
    }

    double jacobi_iteration_2d(const Grid &in, Grid &out, size_t size) {
        double sum_changes = 0;

        for(unsigned i = start_pos; i < end_pos; i++) {
            for(unsigned j = 1; j < size - 1; j++) {
                out(i, j) = (in(i    , j    ) +         // current position
                             in(i - 1, j    ) +         // position northern to current position
                             in(i + 1, j    ) +         // position southern to current position
                             in(i    , j - 1) +         // position western to current position
                             in(i    , j + 1)) / 5;     // position eastern to current position

                sum_changes += std::abs(out(i, j) - in(i, j));
            }
        }

        return sum_changes;
    }

    void synchronization(Grid &vect) {
        int tag = 0;

        // send to right, receive from left
        if(pid == 0) { // left
            MPI::COMM_WORLD.Send(&vect(end_pos - 1, 0), grid_size, MPI::DOUBLE, pid + 1, tag);
        }
        else if(pid == num_processes - 1) { // right
            MPI::COMM_WORLD.Recv(&vect(start_pos - 1, 0), grid_size, MPI::DOUBLE, pid - 1, tag);
        }
        else {  // middle
            MPI::COMM_WORLD.Sendrecv(&vect(end_pos - 1, 0), grid_size, MPI::DOUBLE, pid + 1, tag,
                                     &vect(start_pos - 1, 0), grid_size, MPI::DOUBLE, pid - 1, tag);
        }

        // send to left, receive from right
        if(pid == 0) { // left
            MPI::COMM_WORLD.Recv(&vect(end_pos, 0), grid_size, MPI::DOUBLE, pid + 1, tag);
        }
        else if(pid == num_processes - 1) { // right
            MPI::COMM_WORLD.Send(&vect(start_pos, 0), grid_size, MPI::DOUBLE, pid - 1, tag);
        }
        else {  // middle
            MPI::COMM_WORLD.Sendrecv(&vect(start_pos, 0), grid_size, MPI::DOUBLE, pid - 1, tag,
                                     &vect(end_pos, 0), grid_size, MPI::DOUBLE, pid + 1, tag);
        }
    }

    bool iteration(double epsilon_value, const Grid& in, Grid& out) {
        bool break_out = false;

        double sum_changes = jacobi_iteration_2d(in, out, grid_size);
        double total_changes = 0;

        MPI::COMM_WORLD.Reduce(&sum_changes, &total_changes, 1, MPI::DOUBLE, MPI::SUM, pid_master);

        if(pid == pid_master) {
            if(total_changes < epsilon_value) {
                break_out = true;
            }
        }

        // if master isn't the only participator in the computation, communicate with the others (slaves)
        if(num_processes > 1) {
            MPI::COMM_WORLD.Bcast(&break_out, 1, MPI::BOOL, pid_master);

            synchronization(out);
        }

        return break_out;
    }

    void calculate_stencil(double epsilon_value) {
        double sum_changes = epsilon_value;

        int iterations = 0;
        Grid temp = cells;

        bool break_out;
        while(true) {
            break_out = iteration(epsilon_value, cells, temp);
            iterations++;

            if (break_out) {    // spares unnecessary additional iteration
                cells = temp;
                break;
            }

            break_out = iteration(epsilon_value, temp, cells);
            iterations++;

            if (break_out) {
                break;
            }
        }

        /*if(pid == pid_master) {
           std::cout << "Simulation took " << iterations << " iteration(s)." << std::endl;
        }*/
    }

    // maximum of the absolute values e.g. (-5, 3) -> -5
    static void max_abs(double* in, double* inout, const int* len, MPI::Datatype* dprt) {
        for(int i = 0; i < *len; i++) {
            inout[i] = (std::max(std::abs(in[i]), std::abs(inout[i])) == std::abs(in[i])) ? in[i] : inout[i];
        }
    }

    void synchronize_full() {
        MPI_Op max_abs_op;
        MPI_Op_create((MPI_User_function *) max_abs, 1, &max_abs_op);

        MPI::COMM_WORLD.Allreduce(MPI::IN_PLACE, &cells(0, 0), grid_size * grid_size, MPI::DOUBLE, max_abs_op);

        MPI_Op_free(&max_abs_op);
    }

    const Grid &getCells() const {
        return cells;
    }

    void print_grid() {
        for(unsigned i = 0; i < grid_size; i++) {
            for(unsigned j = 0; j < grid_size; j++) {
                std::cout << cells(i, j) << "\t\t";
            }

            std::cout << "\n";
        }

        std::cout << std::endl;
    }
};

#endif //EXERCISE_07_STENCIL2D_H
