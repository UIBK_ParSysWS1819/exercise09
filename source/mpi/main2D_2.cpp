#include <iostream>
#include "stencil2D_2.h"
#include "../chrono_timer.h"



int main(int argc, char** argv) {
    MPI::Init(argc, argv);  // Initialize MPI

    int pid_master = 0;
    int num_processes = MPI::COMM_WORLD.Get_size();     // Get the total number of processes available.
    int pid = MPI::COMM_WORLD.Get_rank();               // Get the individual process ID.

    // default values
    unsigned int size = 512;
//    unsigned int size = 768;

    double north_boundary = 1.0;
    double east_boundary = 0.5;
    double south_boundary = 0.0;
    double west_boundary = -0.5;

    double epsilon_value = 10.0;
//    double epsilon_value = 100.0;

    if(argc == 7) {
        size = (unsigned) std::stoi(argv[1]);
        north_boundary = std::stod(argv[2]);
        east_boundary = std::stod(argv[3]);
        south_boundary = std::stod(argv[4]);
        west_boundary = std::stod(argv[5]);
        epsilon_value = std::stod(argv[6]);
    }
    else {
        /*
        std::cout << "Warning: No input! Default values will be used! "
                     "Usage: ./stencil2D "
                                "<size> "
                                "<north-boundary> "
                                "<east-boundary> "
                                "<south-boundary> "
                                "<west-boundary>"
                                "<epsilon-value>" << std::endl;
        */
    }

    /*
    std::cout << "size: " << size <<
              "\nnorth boundary: " << north_boundary <<
              "\neast boundary: " << east_boundary <<
              "\nsouth boundary: " << south_boundary <<
              "\nwest boundary: " << west_boundary << 
              "\nepsilon value: " << epsilon_value << std::endl;
    */

    int num_horizontal = (int)sqrt(num_processes);// rounds down
    int num_vertical;
    do {
        num_vertical = num_processes / num_horizontal;
        if (num_horizontal * num_vertical == num_processes && size % num_horizontal == 0 && size % num_vertical == 0) {
            break;
        }
        num_horizontal--;
    } while (num_horizontal != 0);

    if (num_horizontal == 0) {
        if (pid == pid_master) {
            std::cout << "Cannot do amount of processes with given size" << std::endl;
        }
        MPI::Finalize();
        return EXIT_FAILURE;
    }

    Stencil2D stencil2D(pid, pid_master, num_processes, size, num_horizontal, num_vertical, north_boundary, east_boundary, south_boundary, west_boundary);

    ChronoTimer timer("MPI 2D Stencil");

    stencil2D.calculate_stencil(epsilon_value);

    //long time = timer.getElapsedTime_msecs();
    double time = timer.getElapsedTime_secs();

    stencil2D.synchronize_full();

    if (pid == pid_master) {
        //stencil2D.print_grid();
        //std::cout << "Time: " << time << std::endl;
        std::cout << time << std::endl;
    }

    MPI::Finalize();    // Terminate MPI.

    return EXIT_SUCCESS;
}