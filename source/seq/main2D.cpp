#include <iostream>
#include "stencil2D.h"
#include "../chrono_timer.h"


int main(int argc, char** argv) {
    // default values
    unsigned int size = 512;
    double north_boundary = 1.0;
    double east_boundary = 0.5;
    double south_boundary = 0.0;
    double west_boundary = -0.5;
    double epsilon_value = 10.0;
    
    if(argc == 7) {
        size = (unsigned) std::stoi(argv[1]);
        north_boundary = std::stod(argv[2]);
        east_boundary = std::stod(argv[3]);
        south_boundary = std::stod(argv[4]);
        west_boundary = std::stod(argv[5]);
        epsilon_value = std::stod(argv[6]);
    }
    else {
        std::cout << "Warning: No input! Default values will be used! "
                     "Usage: ./stencil2D "
                                "<size> "
                                "<north-boundary> "
                                "<east-boundary> "
                                "<south-boundary> "
                                "<west-boundary> "
                                "<epsilon-value>" << std::endl;
    }

    /*
    std::cout << "size: " << size <<
              "\nnorth boundary: " << north_boundary <<
              "\neast boundary: " << east_boundary <<
              "\nsouth boundary: " << south_boundary <<
              "\nwest boundary: " << west_boundary << 
              "\nepsilon value: " << epsilon_value << std::endl;
    */

    Stencil2D stencil2D(size, north_boundary, east_boundary, south_boundary, west_boundary);

    ChronoTimer timer("Sequential 2D Stencil");

    stencil2D.calculate_stencil(epsilon_value);

    //stencil2D.print_grid();

    double time = timer.getElapsedTime_secs();

    std::cout << time << std::endl;

    return EXIT_SUCCESS;
}