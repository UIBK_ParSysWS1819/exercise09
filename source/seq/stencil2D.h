#ifndef EXERCISE_07_STENCIL2D_H
#define EXERCISE_07_STENCIL2D_H

#include <iostream>
#include <vector>
#include <array>
#include <cmath>

// https://github.com/allscale/allscale_api/wiki/HeatStencil
// http://ipcc.cs.uoregon.edu/lectures/lecture-8-stencil.pdf
// http://www.dais.unive.it/~calpar/New_HPC_course/AA16-17/Project-Jacobi.pdf


class Stencil2D {
    unsigned int grid_size;

    double north_boundary;
    double east_boundary;
    double south_boundary;
    double west_boundary;

    using Grid = std::vector<std::vector<double>>;
    Grid cells;

public:
    Stencil2D(unsigned int size, double north, double east, double south, double west) :
        grid_size(size + 2), north_boundary(north), east_boundary(east), south_boundary(south), west_boundary(west) {    // +2 for boundaries; operating area is then from 1 to N in both dimensions

        cells = Grid(grid_size, std::vector<double>(grid_size));

        // setup boundaries (without corners, because not needed in 5 point stencil)
        set_boundaries();

        // std::cout << "Created a Stencil 2D of size " << grid_size << "x" << grid_size << " (operating size: " << grid_size-2 << "x" << grid_size-2 << ")\n" << std::endl;
    }

    ~Stencil2D() = default;

    void set_boundaries() {
        for(unsigned i = 1; i < grid_size-1; i++) {
            cells[0][i] = north_boundary;
            cells[i][grid_size-1] = east_boundary;
            cells[grid_size-1][i] = south_boundary;
            cells[i][0] = west_boundary;
        }
    }

    double jacobi_iteration_2d(const std::vector<std::vector<double>> &in, std::vector<std::vector<double>> &out, size_t size) {
        double sum_changes = 0;

        for(unsigned i = 1; i < size - 1; i++) {
            for(unsigned j = 1; j < size - 1; j++) {
                out[i][j] = ( in[i  ][j  ] +         // current position
                              in[i-1][j  ] +         // position northern to current position
                              in[i+1][j  ] +         // position southern to current position
                              in[i  ][j-1] +         // position western to current position
                              in[i  ][j+1]) / 5;     // position eastern to current position

                sum_changes += std::abs(out[i][j] - in[i][j]);
            }
        }

        return sum_changes;
    }

    void calculate_stencil(double epsilon_value) {
        double sum_changes = epsilon_value;

        int iterations = 0;
        Grid temp = cells;
        while(sum_changes >= epsilon_value) {   // simulate as long as sum changes is >= 10.0
            jacobi_iteration_2d(cells, temp, grid_size);
            sum_changes = jacobi_iteration_2d(temp, cells, grid_size);
            iterations += 2;
        }

        //std::cout << "Simulation took " << iterations << " iteration(s)." << std::endl;
    }

    const Grid &getCells() const {
        return cells;
    }

    void print_grid() {
        for(unsigned i = 0; i < grid_size; i++) {
            for(unsigned j = 0; j < grid_size; j++) {
                std::cout << cells[i][j] << "\t\t";
            }

            std::cout << "\n";
        }

        std::cout << std::endl;
    }
};

#endif //EXERCISE_07_STENCIL2D_H
