#!/bin/bash
job_str="#!/bin/bash

#$ -N job_file_name_1_numproc_run
#$ -q std.q
#$ -cwd
#$ -o out/out_file_name_1_numproc_run.out
#$ -e out/err_file_name_1_numproc_run.err

# exclusively reserve whatever nodes this is running on
#$ -l excl=true

# reserve 4GB of memory per process
#$ -l h_vmem=\"4G\"

#$ -pe openmpi-8perhost numproc_total

module load gcc/8.2.0
module load openmpi/3.1.1

#mpirun -np numproc_run file_name_2 512 1.0 0.5 0.0 -0.5 10.0
mpirun -np numproc_run file_name_2 768 1.0 0.5 0.0 -0.5 100.0

module unload openmpi/3.1.1
module unload gcc/8.2.0
"

declare -a files=("stencil2D_mpi" "stencil2D_1_mpi" "stencil2D_2_mpi" "stencil2D_3_mpi")

for proc in 1 2 4 8 16 32 64
do

    for name in "${files[@]}"
    do
        new="${job_str//numproc_run/$proc}"
        proc_total=$proc
        if [ $proc -lt 8 ]
        then
            proc_total=8
        fi
        new2="${new//numproc_total/$proc_total}"

        new3="${new2//file_name_2/../bin/$name}"
        echo "${new3//file_name_1/$name}" #| qsub
    done
done