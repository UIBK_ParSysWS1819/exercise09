const fs = require('fs');

const size=512;
const dir ='./out_' + size + '/out/';

let files = fs.readdirSync(dir);

files = files.filter(n => n.endsWith('.out'));

const results = {};
files.forEach(fn => {
    let data = fs.readFileSync(dir + fn, 'utf-8');
    const fs_parts = fn.split('_');
    p = fs_parts.length === 5 ? fs_parts[4] : fs_parts[3]; // 5 for new
    p = p.split('.')[0];
    const name = fs_parts.length === 5 ? 'new_' + fs_parts[2] : 'old';

    if (!results[name]) {
        results[name] = {};
    }
    results[name][p] = data.trim().split('\n');
});
fs.writeFileSync('./results_' + size + '.js', 'var results=' + JSON.stringify(results));


const average = arr => arr.reduce( ( p, c ) => p + Number(c), 0 ) / arr.length;



let md = `|File|1S|2S|4S|8S|16S|32S|64S|
|---:|---:|---:|---:|---:|---:|---:|---:|
`;

Object.entries(results).forEach(([name, data]) => {
    md += `|${name}|${Object.values(data).map(d => average(d)).map(d => `${d.toFixed(4)} s|`).join('')}\n`
});

console.log(md);