#!/bin/bash

for file in "stencil2D_1_mpi" "stencil2D_2_mpi" "stencil2D_3_mpi" "stencil2D_mpi"
do
	echo "rm ${file}/errors/*"
	echo "rm ${file}/times/*"
	rm ${file}/errors/*
	rm ${file}/times/*
done
