#!/bin/bash


echo 'Compiling gcc'

module load gcc/8.2.0
module load openmpi/3.1.1

g++ -std=c++11 -O3 -march=native ../../seq/main2D.cpp -o ../../seq/bin/stencil2D_seq
mpic++ -std=c++11 -O3 -march=native ../../mpi/main2D.cpp -o ../../mpi/bin/stencil2D_mpi
mpic++ -std=c++11 -O3 -march=native ../../mpi/main2D_1.cpp -o ../../mpi/bin/stencil2D_1_mpi
mpic++ -std=c++11 -O3 -march=native ../../mpi/main2D_2.cpp -o ../../mpi/bin/stencil2D_2_mpi
mpic++ -std=c++11 -O3 -march=native ../../mpi/main2D_3.cpp -o ../../mpi/bin/stencil2D_3_mpi

module unload openmpi/3.1.1
module unload gcc/8.2.0

#####################################################################################################################

#echo 'Compiling icc'

#module load intel/15.0
#module load openmpi/3.1.1  # intel/15.0 seems not to work with openmpi/3.1.1
#module load openmpi/2.1.1

#icpc -std=c++11 -O3 -march=native ../seq/main2D.cpp -o ../seq/stencil2D_seq_icc
#mpic++ -std=c++11 -O3 -march=native ../mpi/main2D.cpp -o ../mpi/stencil2D_mpi_icc

#module unload openmpi/3.1.1    # intel/15.0 seems not to work with openmpi/3.1.1
#module unload openmpi/2.1.1
#module unload intel/15.0
