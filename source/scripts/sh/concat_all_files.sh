#!/bin/bash

for file in "stencil2D_1_mpi" "stencil2D_2_mpi" "stencil2D_3_mpi" "stencil2D_mpi"
do
cd $file
	directory=`echo "${PWD##*/}"`
	#echo "filename|size|eps|nslots|median_val_mpi[s]" > ${directory}_output.csv
	echo "size|eps|nslots|median_val_mpi \\[s\\]" > ${directory}_output.csv
	echo "---:|---:|---:|---:" >> ${directory}_output.csv
	
	for filename in times/*.out; do
		#echo "$file $directory $filename"
		more $filename >> ${directory}_output.csv
	done

	for filename in errors/*.err; do
		#echo "$file $directory $filename"
		more $filename >> ${directory}_errors.txt
	done
cd ..
done
