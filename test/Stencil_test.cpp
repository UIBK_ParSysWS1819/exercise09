#include "mpi.h"
#include "../source/mpi/stencil2D_3.h"


void EXPECT_TRUE(bool condition, const std::string &error_message) {
    if (!condition) {
        throw std::runtime_error(error_message);
    }
}

void test_stencil_2d(int pid, int pid_master, int num_processes) {
    unsigned int size = 10;
    double north_boundary = 1.0;
    double east_boundary = 0.0;
    double south_boundary = 0.0;
    double west_boundary = 1.0;
    double epsilon_value = 0.000001;

    double delta = 0.5;

    int num_horizontal = (int) sqrt(num_processes);// rounds down
    int num_vertical;
    do {
        num_vertical = num_processes / num_horizontal;
        if (num_horizontal * num_vertical == num_processes && size % num_horizontal == 0 && size % num_vertical == 0) {
            break;
        }
        num_horizontal--;
    } while (num_horizontal != 0);

    Stencil2D stencil2D(pid, pid_master, num_processes, size, num_horizontal, num_vertical, north_boundary, east_boundary, south_boundary, west_boundary);
    stencil2D.calculate_stencil(epsilon_value);
    stencil2D.synchronize_full();

    // let only the master verify the result
    if(pid == pid_master) {
        auto cells = stencil2D.getCells();  // master should now contain the end result

        for (int i = 0; i < cells.size; i++) {
            double analytical_solution =
                    west_boundary - i * std::abs(west_boundary - east_boundary) / ((double) size + 1);

            // for undefined corner case on left lower corner, which should be 1.0
            double numerical_solution = (i == 0) ? west_boundary : cells(i, i);
            double error = std::abs(numerical_solution - analytical_solution);

            EXPECT_TRUE(-delta <= error && error <= delta, "Test 'Stencil 2D' failed");
        }
    }
}

void test_stencil_2d_fill(int pid, int pid_master, int num_processes) {
    unsigned int size = 10;
    double north_boundary = 1.0;
    double east_boundary = 1.0;
    double south_boundary = 1.0;
    double west_boundary = 1.0;
    double epsilon_value = 0.000001;

    double delta = 0.0001;

    int num_horizontal = (int) sqrt(num_processes);// rounds down
    int num_vertical;
    do {
        num_vertical = num_processes / num_horizontal;
        if (num_horizontal * num_vertical == num_processes && size % num_horizontal == 0 && size % num_vertical == 0) {
            break;
        }
        num_horizontal--;
    } while (num_horizontal != 0);

    Stencil2D stencil2D(pid, pid_master, num_processes, size, num_horizontal, num_vertical, north_boundary, east_boundary, south_boundary, west_boundary);
    stencil2D.calculate_stencil(epsilon_value);
    stencil2D.synchronize_full();

    // let only the master verify the result
    if(pid == pid_master) {
        auto cells = stencil2D.getCells();  // master should now contain the end result
        double analytical_solution = 1.0;

        // leave out boundaries
        for (int i = 1; i < cells.size - 1; i++) {
            double numerical_solution = cells(i, i);
            double error = std::abs(numerical_solution - analytical_solution);

            EXPECT_TRUE(-delta <= error && error <= delta, "Test 'Stencil 2D fill' failed");
        }
    }
}


int main(int argc, char** argv) {
    MPI::Init(argc, argv);  // Initialize MPI

    int pid_master = 0;
    int num_processes = MPI::COMM_WORLD.Get_size();     // Get the total number of processes available.
    int pid = MPI::COMM_WORLD.Get_rank();               // Get the individual process ID.
    
    // Call Tests Here
    test_stencil_2d(pid, pid_master, num_processes);
    test_stencil_2d_fill(pid, pid_master, num_processes);

    if (pid == 0) {
        std::cout << "Tests were successful" << std::endl;
    }

    MPI::Finalize();    // Terminate MPI.

    return EXIT_SUCCESS;
}